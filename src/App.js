import React, { Component } from 'react';
import './App.css'

// Initialisation des constantes globales
const jours = ["DIMANCHE", "LUNDI", "MARDI", "MERCREDI", "JEUDI", "VENDREDI", "SAMEDI"];
const mois = ["JANVIER", "FEVRIER", "mars", "avril", "mai", "juin", "JUILLET", "Aout", "septembre", "octobre", "novembre", "Decembre"];

class App extends Component {

    // Initialisation des variables de state
    state = {
        currentDate: new Date(), // Qui contient la date à afficher. Par défaut contient la date actuelle
        editing: false, // Qui détermine si l'on doit afficher le formulaire de modification ou pas
        edited: false // Qui indique si la date a été modifiée ou pas pour éviter un "timer" inutile
    }

    // Cycle de vie : Lorsque le component est monté. On défini notre "timer"
    componentDidMount() {
        this.timeID = setInterval(() => this.tick(), 1000);
    }

    // Cycle de vie : Lorsque le component est sur le point d'être démonté on arrête le "timer"
    componentWilUnmount() {
        clearInterval(this.timeID);
    }

    // Méthode à éxécuter lors de la modification de la date ainsi que du temps
    handleChange = (e) => {

        // Si après modification le champ est vide, on arrêtre la fonction ici
        if (!e.target.value) return false;

        // On récupère l'instance de la date enregistrée pour se rassurer de ne modifier que ce que l'utilisateur a choisi de modifier (soit le temps, soit la date)
        const newDate = this.state.currentDate;

        // SI l'utilisateur choisi de modifier le temps on le récupère. Le temps se présente sous la forme d'une chaîne de caractère "heure:minutes". On sépare donc avec la méthde .split() les heures et minutes dans un tableau et on gère
        if (e.target.name === "mytime") {
            const heumin = e.target.value.split(":");
            newDate.setHours(heumin[0], heumin[1]);
        } else {
            // Si l'utilisateur choisi de modifier la date, elle se présente ainsi "annee-mois-jour", on utilise alours le "-" comme séparateur et puis on défini les éléments comme tu vois là :-)
            const jou_moi_an = e.target.value.split("-");
            newDate.setFullYear(parseInt(jou_moi_an[0]))
            newDate.setMonth(parseInt(jou_moi_an[1]) - 1)
            newDate.setDate(parseInt(jou_moi_an[2]))
        }

        // Et là, il suffit d'affecter notre nouvelle date dans le state et marquer que nous avons effectué une modification pour stopper le timer
        this.setState({ currentDate: newDate, edited: true })

    }

    // Permet de basculer en mode édition ou le cacher
    toggleMode = (e) => {
        this.setState({ editing: e.target.checked });
    }

    // Permet de réinitialiser les changements. On place la date actuelle et on met l'indicateur de changement à false pour que celui-ci se poursuive
    resetChanges = () => {
        this.setState({ currentDate: new Date(), edited: false });
    }

    // Le gars ci va soloment s'éxécuter à chaque seconde pour vérifier que le temps affiché soit bien à jour
    tick() {
        if (!this.state.edited) {
            this.setState({
                currentDate: new Date()
            });
        }
    }

    // Tout est dans le nom
    ajouteZero = (n) => n > 9 ? n : "0" + n;

    // Ici je ne commente pas
    render() {

        // Pour éviter d'écrire this.state.currentDate chaque temps chaque temps, mieux je passe moi par la déconstruction d'objet. Là ça va créer une constante au nom de currentDate. "currentDate" est une clé de l'objet this.state
        const { currentDate } = this.state;

        // L'autre ci là c'est seulement le fax hein... ya même rien à dire. C'est évident
        const day = currentDate.getDay();
        const date = this.ajouteZero(currentDate.getDate());
        const month = currentDate.getMonth();
        const year = currentDate.getFullYear();

        const hour = this.ajouteZero(currentDate.getHours());
        const minutes = this.ajouteZero(currentDate.getMinutes());

        // Encore plus évident qu'une évidence
        let periode = "MATIN";
        if (hour > 12 && hour < 16) {
            periode = "APRES-MIDI";
        } else if (hour > 16) {
            periode = "SOIR";
        }

        // On ne commente pas le HTML :-(
        return (
            <div className="container">

                <div>

                    <form className="form">

                        <div>
                            <input type="checkbox" id="editing" onChange={this.toggleMode} />
                            <label htmlFor="editing" className={this.state.editing ? 'active' : ""}>
                                {/* Syntaxe de conditions ternaires */}
                                {!this.state.editing ? "Afficher le formulaire" : "Cacher le formulaire"}
                            </label>
                        </div>

                        {/* Affichage conditionnel */}
                        {this.state.editing && (<div className="fields">
                            <input type="date" name="mydate" defaultValue={`${year}-${this.ajouteZero(month + 1)}-${date}`} onChange={this.handleChange} />
                            <input type="time" name="mytime" defaultValue={`${hour}:${minutes}`} onChange={this.handleChange} />
                        </div>)}

                    </form>

                    <div className="firstblock">

                        <div className="secondblock">
                            <span className="day">{jours[day]}</span><br />
                            <span className="date">{periode}</span>
                            <div className="hour">{hour}:{minutes}</div>
                            <div className="date">{date} {mois[month].toUpperCase()} {year}</div>
                            <button className="reset" onClick={this.resetChanges}>Annuler les modifications</button>
                        </div>


                    </div>

                </div>
            </div>

        );
    }
}

export default App;
